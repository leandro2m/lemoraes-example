const express = require('express');
const app = express();
const path = require('path');
const methodOverride = require('method-override');
const cors = require('cors');


// Run the app by serving the static files
// in the dist directory
app.use(express.static(__dirname + '/app'));
app.use(methodOverride('X-HTTP-Method-Override'))
app.use(cors({origin: '*'}));

// Start the app by listening on the default
// Heroku port

// For all GET requests, send back index.html
// so that PathLocationStrategy can be used
app.get('/', function(req, res) {
  res.send('route does not exist');
});
app.get('/app', function(req, res) {
  res.sendFile('index.html', { root: __dirname });
});

app.get('/example-nodejs', function (req, res) {
	res.sendFile('index.html', { root: __dirname })
});

app.get('/failure', function(req, res) {
    console.log('Error Failure 0x9019838757. Press Ctrl + F13 to continue');
    process.exit();
    
  });
app.get('*', function(req, res) {
    res.redirect('/');
  });
app.listen(process.env.PORT || 8088);
console.log('Listening port 8088');