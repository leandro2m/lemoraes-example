FROM leandro2m/alpine:nodejs
ADD app /app
COPY app.js /app.js
COPY package.json /package.json
RUN npm install
EXPOSE 8088
CMD npm start
